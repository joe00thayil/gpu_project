#include <stdio.h>
//#include "kernels.h"
#include <iostream>
#include <cuda_runtime.h>
#include <cuda.h>

#define WARPSIZE 32

__device__ volatile int32_t maxColorVal(0); // Stores the current maximum value of the colors used so far
__device__ volatile int32_t oneSCCcount(0);
__device__ volatile int32_t sccCount(0); // total count of SCCs in G
__device__ volatile int32_t maxSCCSize(1); // Size of the maximum sized SCC, Initialized to 1

__global__ void Trim1(int32_t *color, int8_t *tags, int8_t *transTags, int32_t *nodes, int32_t *transNodes, int32_t N)
{
	int32_t id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < N)
	{
	int32_t a = nodes[id + 1] - nodes[id]; // Outdegree of vertex id in G
	int32_t b =  transNodes[id + 1] - transNodes[id]; // Outdegree of vertex id in G transpose == Indegree in G
	printf("id %d\ta %d\t b %d\n",id,a,b);
	if((a == 0 || b == 0) && ((tags[id] & 1) == 0))
	{
		// 1-SCC identified
	//	color[id] = atomicAdd((int32_t*)&maxColorVal, 1);
		tags[id] = tags[id] | 1; // LSB of tag represents eliminated bit. Setting it to 1
		transTags[id]=transTags[id] | 1;
		atomicAdd((int32_t*)&oneSCCcount, 1);
		atomicAdd((int32_t*)&sccCount, 1);
	}
	}
}

// While calculating inDegree and outDegree. consider if the end vertex is eliminbated or not
__global__ void TrimTwo(int32_t *color, int8_t *tags, int8_t *transTags,  int32_t* nodes, int32_t* edges, int32_t* transNodes, int32_t* transEdges, int32_t V)
{
	unsigned int n = blockIdx.x * blockDim.x + threadIdx.x;
	if(n < V)
	{
		int inDegree_n = transNodes[n+1] - transNodes[n];
		int outDegree_n = nodes[n+1] - nodes[n];
		// Identifying if n is part of a 2-SCC
		if((tags[n] & 1)==0)
		{
		if(inDegree_n == 1)
		{
			int k = transEdges[(transNodes[n])]; // k is the only in-neighbour of n
			if(color[n] == color[k]) // n and k are in the same subgraph
			{
				int inDegree_k = transNodes[k+1] - transNodes[k];
				if(inDegree_k == 1)
				{
					int inNbrOf_k = transEdges[(transNodes[k])];
					if(inNbrOf_k == n) // n is the only in-neighbour of k
					{
						// k and n form a 2-SCC
						tags[n] = tags[n] | 1; // Eliminated bit to 1
						tags[k] = tags[k] | 1;
						transTags[n] = transTags[n] | 1; // Eliminated bit to 1
						transTags[k] = transTags[k] | 1;
						atomicAdd((int32_t*)&sccCount, 1);
						atomicMax((int32_t*)&maxSCCSize, 2);	
					}

				}

			}
		}
		else if(outDegree_n == 1)
		{
			int k = edges[(nodes[n])]; // k is the only out-neighbour of n
			if(color[n] == color[k]) // n and k are in the same subgraph
			{
				int outDegree_k = nodes[k+1] - nodes[k];
				if(outDegree_k == 1)
				{
					int outNbrOf_k = edges[(nodes[k])];
					if(outNbrOf_k == n) // n is the only out-neighbour of k
					{
						// k and n form a 2-SCC
						tags[n] = tags[n] | 1; // Eliminated bit to 1
						tags[k] = tags[k] | 1;
						transTags[n] = transTags[n] | 1; // Eliminated bit to 1
						transTags[k] = transTags[k] | 1;
						atomicAdd((int32_t*)&sccCount, 1);
						atomicMax((int32_t*)&maxSCCSize, 2);
					}
				}
			}
		}
		}
	}
}



__global__ void PivotSelection(int32_t *color, int8_t *tags, int32_t *pivots, int32_t *nodes, int32_t *transNodes, int32_t N)
{
	int32_t id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < N)
	{
	if((tags[id] & 1) == 0) // The vertex is not yet eliminated
	{
		int32_t subGraphId = color[id];
		int32_t currentPivot, old, curProd;
		int32_t newProd = (nodes[id + 1] - nodes[id]) * (transNodes[id + 1] - transNodes[id]); // product of in and out degrees of the new candidate for pivot
		do{
			currentPivot = pivots[subGraphId % N];
			curProd = (nodes[currentPivot + 1] - nodes[currentPivot]) *  (transNodes[currentPivot + 1] - transNodes[currentPivot]); // product of in and out degrees for currentPivot
			if(currentPivot == -1 || newProd > curProd)
			{
				old = atomicCAS(&(pivots[subGraphId % N]), currentPivot, id); 
			}
			else
			{
				old = currentPivot;
		
			}
		}while(old != currentPivot);

	}
	}

}

__global__ void setVisited(int32_t *pivots, int8_t *tags, int8_t *transTags,int32_t N)
{
	int32_t id=threadIdx.x+blockIdx.x*blockDim.x;
	if(id < N)
	{
	if(pivots[id]!=-1)
	{
		tags[pivots[id]] = tags[pivots[id]] | 2;
		transTags[pivots[id]] = transTags[pivots[id]] | 2;
	}
	}
}

__global__ void warpCentricFWD(int32_t *nodes , int32_t *edges , int32_t *color , int8_t *tags , int32_t *propagate , int32_t N)
{
	int32_t v = blockIdx.x * blockDim.x + threadIdx.x;
	if(v < N*WARPSIZE)
	{	

		int32_t w = v / WARPSIZE;
		int32_t eliminated_w = tags[w] & 1;
		int32_t visited_w =  tags[w] & 2;
//		printf("warpCentric id %d\teli %d\tvisited %d\n",w,eliminated_w,visited_w);
		if(eliminated_w == 0 && visited_w == 2)
		{
			int32_t nbrCount = nodes[w+1] - nodes[w];
			int32_t nbrBegin = nodes[w];
			for(int32_t i = (v % WARPSIZE); i < nbrCount; i += WARPSIZE)
			{
				int32_t u = edges[nbrBegin + i];
				int32_t eliminated_u = tags[u] & 1;
				int32_t visited_u = tags[u] & 2;
				if(eliminated_u == 0 && visited_u == 0 && color[u] == color[w])
				{
					tags[u] = tags[u] | 2; // Set visited to true
					*propagate = 1;
				}
			}
		}
	}
}

__global__ void FWD_kernel(int32_t *nodes , int32_t *edges , int32_t *color , int8_t *tags , int32_t *propagate , int32_t N)
{
//helooo
	
	int32_t v=threadIdx.x+blockIdx.x*blockDim.x;
	if(v<N)
	{
		int8_t visited_v=(tags[v]&2);
		int8_t eliminated_v=(tags[v]&1);
		int32_t nbrCnt,nbrBegin,u;
		//printf("id %d\teli %d\tvisited %d\n",v,eliminated_v,visited_v);
		if( (eliminated_v==0) && (visited_v==2) )
		{

			nbrCnt=nodes[v+1]-nodes[v];
			nbrBegin=nodes[v];
			int8_t visited_u,eliminated_u;
			for(int i=0;i<nbrCnt;i++)
			{
				u=edges[nbrBegin+i];
				eliminated_u=(tags[u]&1);
				visited_u=(tags[u]&2);
				if( (eliminated_u==0) && (visited_u==0) && (color[u]==color[v]) )
				{
			//		std::cout<<"helloo\n";
					tags[u]=tags[u]|2;
					*propagate=1;

				}
			}
		}
	}
}




// NOTE : Before calling FWD and BWD kernels, clear visited bit field in tags array. Also, before calling pivot selection, reset pivots array to -1 always
__global__ void Update(int32_t *color, int8_t *tags, int8_t *transTags, int32_t *terminate, int32_t N)
{
	int32_t u = blockIdx.x * blockDim.x + threadIdx.x;
	if(u < N && (tags[u] & 1) == 0) // Proceed only if the vertex u is not yet eliminated
	{
		int32_t i = color[u];
		if(u == i) // u is a pivot
		{
			atomicAdd((int32_t*)&sccCount, 1); /*TO-DO: Check how to track max sized SCC's size. This may be a 1-SCC too. Need to add to oneSCCcount then*/
		}
		// Second LSB in tags array indicates visited flag of a vertex
		if((tags[u] & 2) == 2 && (transTags[u] & 2) == 2) /* u is in both fwd and bwd reachability closure of one pivot => It is part of the SCC containing its pivot */
		{
			tags[u] = tags[u] | 1; // Setting eliminated bit of u to 1
			transTags[u] = transTags[u] | 1;
		}
		else
		{
			terminate = false; // a new subgraph is formed. Needs further processing 
			if((tags[u] & 2) == 0 && (transTags[u] & 2) == 0) // u is unreachable from its pivot
				color[u] = 3 * i;
			else if((tags[u] & 2) == 2 && (transTags[u] & 2) == 0) // u is only forward reachable from its pivot
				color[u] = 3 * i + 1;
			else if((tags[u] & 2) == 0 && (transTags[u] & 2) == 2)// u is only backward reachable from its pivot
				color[u] = 3 * i + 2;
		}
	}
}





__global__ void kernel_print()
{
	printf("scc count=%d\nmaxColorVal%d\n oneSCCcount=%d\nmaxSCCSize%d\n",sccCount,maxColorVal,oneSCCcount,maxSCCSize);
}




int main()
{
	int32_t N,E;
	std::cin>>N;
	std::cin>>E;
	int32_t *nodes=(int32_t *)malloc(sizeof(int32_t)*(N+1));
	int32_t *edges=(int32_t *)malloc(sizeof(int32_t)*E);
	int32_t *transNodes=(int32_t *)malloc(sizeof(int32_t)*(N+1));
	int32_t *transEdges=(int32_t *)malloc(sizeof(int32_t)*E);
	int32_t *colors=(int32_t *)malloc(sizeof(int32_t)*N);
	int32_t *pivots=(int32_t *)malloc(sizeof(int32_t)*N);
	int8_t *tags=(int8_t *)malloc(sizeof(int8_t)*N);
	int8_t *transTags=(int8_t *)malloc(sizeof(int8_t)*N);

	for(int32_t i=0;i<N;i++)
	{	pivots[i]=-1;
		nodes[i]=-1;
		transNodes[i]=-1;
		colors[i]=0;
		tags[i]=0;
		transTags[i]=0;
	}
	int32_t e=0,n1,n2;
	for(int32_t i=0;i<E;i++)
	{
		std::cin>>n1;
		std::cin>>n2;
		n1--;
		n2--;
		if(nodes[n1]==-1)
			nodes[n1]=e;

		edges[e]=n2;
		e++;
	}
	for(int32_t i=0;i<N;i++)
	{
		if(nodes[i]==-1)
		{
			int32_t j=i+1;
			while(j<N&&(nodes[j]==-1))
				j++;
			if(j<N)
				nodes[i]=nodes[j];
			else
				nodes[i]=E;
		}
	}
	e=0;
	for(int32_t i=0;i<E;i++)
	{
		std::cin>>n1;
		std::cin>>n2;
		n1--;
		n2--;
		if(transNodes[n2]==-1)
			transNodes[n2]=e;
		transEdges[e]=n1;
		e++;
	}
	for(int32_t i=0;i<N;i++)
	{
		if(transNodes[i]==-1)
		{
			int32_t j=i+1;
			while(j<N&&(transNodes[j]==-1))
				j++;
			if(j<N)
				transNodes[i]=transNodes[j];
			else
				transNodes[i]=E;
		}
	}
transNodes[N]=E;
nodes[N]=E;

//PRINTING
	/*
	for(int32_t i=0;i<N-1;i++)
	  {
	  int32_t offset=nodes[i+1]-nodes[i];
	  for(int32_t j=0;j<offset;j++)
	  {std::cout<<i<<"\t"<<edges[nodes[i]+j]<<"\n";
	  }
	  }
	  if(nodes[N-1]<E)
	  {
	  int i=nodes[N-1];
	//	std::cout<<i<<"\t"<<edges[i]<<"\n";
	while(i<E)
	{
	std::cout<<(N-1)<<"\t"<<edges[i]<<"\n";
	i++;
	}
	}
	for(int32_t i=0;i<N-1;i++)
	  {
	  int32_t offset=transNodes[i+1]-transNodes[i];
	  for(int32_t j=0;j<offset;j++)
	  {std::cout<<i<<"\t"<<transEdges[transNodes[i]+j]<<"\n";
	  }
	  }
	  if(transNodes[N-1]<E)
	  {
	  int i=transNodes[N-1];
	//	std::cout<<i<<"\t"<<edges[i]<<"\n";
	while(i<E)
	{
	std::cout<<(N-1)<<"\t"<<transEdges[i]<<"\n";
	i++;
	}
	}

*/
	//Kernel call
	int32_t *d_propagateForward,*d_terminate,*d_propagateBackward,*d_pivots,*d_nodes,*d_edges,*d_transNodes,*d_transEdges,*d_colors;
	int32_t *propagateForward=(int32_t *)malloc(sizeof(int32_t));
	int32_t *propagateBackward=(int32_t *)malloc(sizeof(int32_t));
	int32_t *terminate=(int32_t *)malloc(sizeof(int32_t));
	int8_t *d_tags,*d_transTags;
	cudaMalloc(&d_terminate, sizeof(int32_t));
	cudaMalloc(&d_propagateForward, sizeof(int32_t));
	cudaMalloc(&d_propagateBackward, sizeof(int32_t));
	cudaMalloc(&d_nodes, (N+1)*sizeof(int32_t));
	cudaMalloc(&d_transNodes, (N+1)*sizeof(int32_t));
	cudaMalloc(&d_transEdges, E*sizeof(int32_t));
	cudaMalloc(&d_edges, E*sizeof(int32_t));
	cudaMalloc(&d_colors, N*sizeof(int32_t));
	cudaMalloc(&d_pivots, N*sizeof(int32_t));
	cudaMalloc(&d_tags, N*sizeof(int8_t));
	cudaMalloc(&d_transTags, N*sizeof(int8_t));
	cudaMemcpy(d_nodes, nodes,(N+1)*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_colors,colors,N*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_pivots,pivots,N*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tags,tags,N*sizeof(int8_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_transTags,transTags,N*sizeof(int8_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_edges,edges,E*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_transNodes, transNodes,(N+1)*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_transEdges, transEdges,E*sizeof(int32_t), cudaMemcpyHostToDevice);
	*propagateForward=1;
	*propagateBackward=1;
	*terminate=0;

	Trim1<<<(N/1024)+1,1024>>>(d_colors,d_tags,d_transTags,d_nodes,d_transNodes, N);
	cudaDeviceSynchronize();
	PivotSelection<<<(N/1024)+1,1024>>>(d_colors , d_tags , d_pivots, d_nodes , d_transNodes , N);
	cudaDeviceSynchronize();
	cudaMemcpy(pivots,d_pivots,sizeof(int32_t)*N, cudaMemcpyDeviceToHost);
	
	
/*	for(int i=0;i<N;i++)
	{
		printf("i %d\tpivot %d\n",i,pivots[i]);
	}*/
	setVisited<<<(N/1024)+1,1024>>>(d_pivots,d_tags,d_transTags,N);
	cudaDeviceSynchronize();
//creating stream
	cudaStream_t S1,S2;
	cudaStreamCreate(&S1);
	cudaStreamCreate(&S2);



	while( *propagateForward || *propagateBackward)	
	{
		if(*propagateForward)
		{	
			cudaMemset(d_propagateForward, 0,sizeof(int32_t));
//			FWD_kernel<<<(N/1024)+1,1024,0,S1>>>(d_nodes,d_edges,d_colors,d_tags,d_propagateForward,N);
			warpCentricFWD<<<((N*WARPSIZE)/1024)+1,1024,0,S1>>>(d_nodes,d_edges,d_colors,d_tags,d_propagateForward,N);
		}
		if(*propagateBackward)
		{	
			cudaMemset(d_propagateBackward, 0,sizeof(int32_t));
//			FWD_kernel<<<(N/1024)+1,1024,0,S2>>>(d_transNodes,d_transEdges,d_colors,d_transTags,d_propagateBackward,N);
			warpCentricFWD<<<((N*WARPSIZE)/1024)+1,1024,0,S2>>>(d_transNodes,d_transEdges,d_colors,d_transTags,d_propagateBackward,N);
		}

		cudaDeviceSynchronize();
		cudaMemcpy(propagateForward , d_propagateForward , sizeof(int32_t), cudaMemcpyDeviceToHost);
		cudaMemcpy(propagateBackward , d_propagateBackward , sizeof(int32_t), cudaMemcpyDeviceToHost);
	}

	Update<<<(N/1024)+1,1024>>>(d_colors,d_tags,d_transTags, d_terminate, N);
	cudaDeviceSynchronize();
printf("*******************************************\n");
	TrimTwo<<<(N/1024)+1,1024>>>(d_colors, d_tags,d_transTags, d_nodes, d_edges, d_transNodes, d_transEdges, N);
	cudaDeviceSynchronize();
	//Trim1<<<(N/1024)+1,1024>>>(d_colors,d_tags,d_transTags,d_nodes,d_transNodes, N);
	//cudaDeviceSynchronize();
	
	cudaMemcpy(tags,d_tags,sizeof(int8_t)*N, cudaMemcpyDeviceToHost);
	cudaMemcpy(transTags,d_transTags,sizeof(int8_t)*N, cudaMemcpyDeviceToHost);
	cudaMemcpy(colors,d_colors,sizeof(int32_t)*N, cudaMemcpyDeviceToHost);
	kernel_print<<<1,1>>>();
	cudaDeviceSynchronize();
	for(int i=0;i<N;i++)
	{
		std::cout<<"node "<<i<<"\teli  "<<(tags[i]&1)<<"\tcolor "<<colors[i]<<"\n";

	}
/*	std::cout<<"****************************************************\n";
	for(int i=0;i<N;i++)
	{
	
		std::cout<<"node "<<i<<"\tvisited trans"<<(transTags[i]&2)<<"\tcolor "<<colors[i]<<"\n";

	}
*/
	//	for(int32_t i=0;i<E;i++)
	//		edges[i]=i;
	std::cout<<"Number of nodes:"<<N<<"\nNumber of edges:"<<E<<"\n";
}
