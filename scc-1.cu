#include <stdio.h>
//#include "kernels.h"
#include <iostream>
#include <cuda_runtime.h>
#include <cuda.h>

#define WARPSIZE 32

__device__ volatile int32_t maxColorVal(0); // Stores the current maximum value of the colors used so far
__device__ volatile int32_t oneSCCcount(0);
__device__ volatile int32_t sccCount(0); // total count of SCCs in G
__device__ volatile int32_t maxSCCSize(1); // Size of the maximum sized SCC, Initialized to 1
__device__ volatile int32_t mutex(0); 


// Returns the count of uneliminated out-neighbours of u
__device__ int32_t outDegree(int32_t u, int8_t *tags, int32_t *nodes, int32_t *edges)
{
	int32_t count = 0;
	int32_t nbrBegin = nodes[u];
	int32_t nbrCount = nodes[u+1] -  nodes[u];
	int32_t v;
	for(int32_t i = 0; i < nbrCount; i++)
	{
		v = edges[nbrBegin + i];
		if((tags[v] & 1) == 0) // v is not yet eliminated
			count++;
	}
	return count;
}

// Returns the count of uneliminated in-neighbours of u
__device__ int32_t inDegree(int32_t u, int8_t *transTags, int32_t *transNodes, int32_t *transEdges)
{
	
	int32_t count = 0;
	int32_t nbrBegin = transNodes[u];
	int32_t nbrCount = transNodes[u+1] -  transNodes[u];
	int32_t v;
	for(int32_t i = 0; i < nbrCount; i++)
	{
		v = transEdges[nbrBegin + i];
		if((transTags[v] & 1) == 0) // v is not yet eliminated
			count++;
	}
	return count;
}

// Returns the sole in- OR out- neighbour of u. Whether the neighbour is an in- or out- one depends upon the arguments passed - G or G transpose
__device__ int32_t getSoleNbr(int32_t u, int8_t *tags, int32_t *nodes, int32_t *edges)
{
	int nbrBegin = nodes[u];
        int32_t nbrCount = nodes[u+1] -  nodes[u];
        int32_t v = -1;
        for(int32_t i = 0; i < nbrCount; i++)
        {
                v = edges[nbrBegin + i];
                if((tags[v] & 1) == 0) // v is not yet eliminated
                        return v;
        }
	return v; // Note: If no uneliminated neighbour present, return value is -1
}




__global__ void Trim1(int32_t *color, int8_t *tags, int8_t *transTags, int32_t *nodes, int32_t *transNodes, int32_t *edges, int32_t *transEdges, int32_t N)
{
	int32_t id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < N && ((tags[id] & 1) == 0)) // check that the vertex id is not yet eliminated
	{
	int32_t a = outDegree(id, tags, nodes, edges); 
	int32_t b =  inDegree(id, transTags, transNodes, transEdges); 
//	printf("id %d\ta %d\t b %d\n",id,a,b);
	if(a == 0 || b == 0)
	{
		// 1-SCC identified
		color[id] = -1; // Setting color of eliminated vertx to -1
		tags[id] = tags[id] | 1; // LSB of tag represents eliminated bit. Setting it to 1
		transTags[id]=transTags[id] | 1;
		atomicAdd((int32_t*)&oneSCCcount, 1);
		atomicAdd((int32_t*)&sccCount, 1);
	}
	}
}
// While calculating inDegree and outDegree. consider if the end vertex is eliminbated or not
__global__ void TrimTwo(int32_t *color, int8_t *tags, int8_t *transTags,  int32_t* nodes, int32_t* edges, int32_t* transNodes, int32_t* transEdges, int32_t V)
{

	int32_t m = blockIdx.x * blockDim.x + threadIdx.x;
	if(m < V * WARPSIZE && (m % WARPSIZE) == 0) // handle only if n is not yet eliminated
	{
		int32_t n=m/WARPSIZE;
		if((tags[n] & 1) == 0)
		{
		int32_t outDegree_n = outDegree(n, tags, nodes, edges);
		int32_t inDegree_n = inDegree(n, transTags, transNodes, transEdges);
//printf("id %d\toutdeg %d\tindeg %d\n",n,outDegree_n,inDegree_n);		// Identifying if n is part of a 2-SCC

		if(inDegree_n == 1)
		{
			int32_t k = getSoleNbr(n, transTags, transNodes, transEdges); // k is the only uneliminated in-neighbour of n
			if(k != -1 && color[n] == color[k]) // n and k are in the same subgraph
			{
				int32_t inDegree_k = inDegree(k, transTags, transNodes, transEdges);
				if(inDegree_k == 1)
				{
					int32_t inNbrOf_k = getSoleNbr(k, transTags, transNodes, transEdges);
					if(inNbrOf_k == n) // n is the only in-neighbour(not yet eliminated) of k
					{
					int32_t old;
						do{
							old = atomicCAS((int32_t*)&mutex, 0, 1);
						}while(old == 1);
						
						printf("mutex %d\t id %d\n",mutex,n);
						if((tags[n] & 1) == 0 && (tags[k] & 1) == 0)
						{
						// k and n form a 2-SCC
						tags[n] = tags[n] | 1; // Eliminated bit to 1
						tags[k] = tags[k] | 1;
						transTags[n] = transTags[n] | 1; // Eliminated bit to 1
						transTags[k] = transTags[k] | 1;
						color[k] = -1;
						color[n] = -1;
						atomicAdd((int32_t*)&sccCount, 1);
						atomicMax((int32_t*)&maxSCCSize, 2);
						}
						mutex = 0;
					}

				}

			}
		}
		else if(outDegree_n == 1)
		{
			int32_t k = getSoleNbr(n, tags, nodes, edges); // k is the only out-neighbour of n, and k is not yet eliminated too
			if(k != -1 && color[n] == color[k]) // n and k are in the same subgraph
			{
				int32_t outDegree_k = outDegree(k, tags, nodes, edges);
				if(outDegree_k == 1)
				{
					int32_t outNbrOf_k = getSoleNbr(k, tags, nodes, edges);
					if(outNbrOf_k == n) // n is the only uneliminated out-neighbour of k
					{
						// k and n form a 2-SCC
						tags[n] = tags[n] | 1; // Eliminated bit to 1
						tags[k] = tags[k] | 1;
						transTags[n] = transTags[n] | 1; // Eliminated bit to 1
						transTags[k] = transTags[k] | 1;
						color[k] = -1;
						color[n] = -1;
						atomicAdd((int32_t*)&sccCount, 1);
						atomicMax((int32_t*)&maxSCCSize, 2);
					}
				}
			}
		}
	}
	}
}


__global__ void WCC_K1(int32_t *color, int8_t *tags, int8_t *transTags,  int32_t* nodes, int32_t* edges, int32_t* transNodes, int32_t* transEdges, int32_t* wcc, int32_t* continueWcc, int32_t V)
{
	int32_t n = blockIdx.x * blockDim.x + threadIdx.x;
        if(n < V && (tags[n] & 1) == 0) // handle only if n is not yet eliminated
	{
		int32_t k;
		int32_t end = nodes[n+1] - nodes[n];
		for(int32_t i = nodes[n]; i < end; i++)
		{
			k = edges[i];
			if(color[k] == color[n] && wcc[k] < wcc[n])
			{
				wcc[n] = wcc[k];
				*continueWcc = 1;
			}
		}
		// Do the same for all in-neighbours too
		end = transNodes[n+1] - transNodes[n];
		for(int32_t i = transNodes[n]; i < end; i++)
		{
			k = transEdges[i];
			if(color[k] == color[n] && wcc[k] < wcc[n])
			{
				wcc[n] = wcc[k];
				*continueWcc = 1;
			}
		}
	}
}


__global__ void WCC_K2(int8_t *tags, int32_t *wcc, int32_t *continueWcc, int32_t V)
{
	
		int32_t n = blockIdx.x * blockDim.x + threadIdx.x;
        if(n < V && (tags[n] & 1) == 0) // handle only if n is not yet eliminated
	{
		int32_t k = wcc[n];
		if(k != n && k != wcc[k])
		{
			wcc[n] = wcc[k];
			*continueWcc = 1;
		}
	}
}

__global__ void PivotSelection(int32_t *color, int8_t *tags,int8_t *transTags, int32_t *pivots, int32_t *nodes, int32_t *transNodes,int32_t *edges, int32_t *transEdges, int32_t N)
{
	int32_t id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < N)
	{
	if((tags[id] & 1) == 0) // The vertex is not yet eliminated
	{
		int32_t subGraphId = color[id];
		int32_t currentPivot, old, curProd;
		int32_t newProd = outDegree(id, tags, nodes, edges) * inDegree(id, transTags, transNodes, transEdges);  // product of in and out degrees of the new candidate for pivot
		do{
			currentPivot = pivots[subGraphId % N];
			curProd = outDegree(currentPivot, tags, nodes, edges) * inDegree(currentPivot, transTags, transNodes, transEdges); // product of in and out degrees for currentPivot
			if(currentPivot == -1 || newProd > curProd)
			{
				old = atomicCAS(&(pivots[subGraphId % N]), currentPivot, id); 
			}
			else
			{
				old = currentPivot;
		
			}
		}while(old != currentPivot);

	}
	}

}

// Setting the visited flag of all pivots to true
__global__ void setVisited(int32_t *pivots, int8_t *tags, int8_t *transTags,int32_t N)
{
	int32_t id=threadIdx.x+blockIdx.x*blockDim.x;
	if(id < N)
	{
	if(pivots[id]!=-1)
	{
		tags[pivots[id]] = tags[pivots[id]] | 2;
		transTags[pivots[id]] = transTags[pivots[id]] | 2;
	}
	}
}

__global__ void warpCentricFWD(int32_t *nodes , int32_t *edges , int32_t *color , int8_t *tags , int32_t *propagate , int32_t N)
{
	int32_t v = blockIdx.x * blockDim.x + threadIdx.x;
	if(v < N * WARPSIZE)
	{	

		int32_t w = v / WARPSIZE;
		int32_t eliminated_w = tags[w] & 1;
		int32_t visited_w =  tags[w] & 2;
//		printf("warpCentric id %d\teli %d\tvisited %d\n",w,eliminated_w,visited_w);
		if(eliminated_w == 0 && visited_w == 2)
		{
			int32_t nbrCount = nodes[w+1] - nodes[w];
			int32_t nbrBegin = nodes[w];
			for(int32_t i = (v % WARPSIZE); i < nbrCount; i += WARPSIZE)
			{
				int32_t u = edges[nbrBegin + i];
				int32_t eliminated_u = tags[u] & 1;
				int32_t visited_u = tags[u] & 2;
				if(eliminated_u == 0 && visited_u == 0 && color[u] == color[w])
				{
					tags[u] = tags[u] | 2; // Set visited to true
					*propagate = 1;
				}
			}
		}
	}
}

__global__ void FWD_kernel(int32_t *nodes , int32_t *edges , int32_t *color , int8_t *tags , int32_t *propagate , int32_t N)
{
//helooo
	
	int32_t v = threadIdx.x + blockIdx.x * blockDim.x;
	if(v < N)
	{
		int8_t visited_v = (tags[v] & 2);
		int8_t eliminated_v = (tags[v] & 1);
		int32_t nbrCnt, nbrBegin, u;
		//printf("id %d\teli %d\tvisited %d\n",v,eliminated_v,visited_v);
		if( (eliminated_v == 0) && (visited_v == 2) ) // v is visited, and not yet eliminated
		{
			nbrCnt = nodes[v+1] - nodes[v];
			nbrBegin = nodes[v];
			int8_t visited_u, eliminated_u;
			for(int i = 0; i < nbrCnt; i++)
			{
				u = edges[nbrBegin+i];
				eliminated_u = (tags[u] & 1);
				visited_u = (tags[u] & 2);
				// Check if u is not yet eliminated and not yet visited, but having same color as v
				if( (eliminated_u == 0) && (visited_u == 0) && (color[u] == color[v]) )
				{
					tags[u] = tags[u] | 2; // Mark u as visited
					*propagate = 1;

				}
			}
		}
	}
}




// NOTE : Before calling FWD and BWD kernels, clear visited bit field in tags array. Also, before calling pivot selection, reset pivots array to -1 always
__global__ void Update(int32_t *color, int8_t *tags, int8_t *transTags, int32_t *terminate, int32_t *pivots, int32_t N)
{
	int32_t u = blockIdx.x * blockDim.x + threadIdx.x;
	if(u < N && (tags[u] & 1) == 0) // Proceed only if the vertex u is not yet eliminated
	{
		int32_t i = color[u];
		if(u == pivots[i]) // u is a pivot
		{
			atomicAdd((int32_t*)&sccCount, 1); /*TO-DO: Check how to track max sized SCC's size. This may be a 1-SCC too. Need to add to oneSCCcount then*/
		}
		// Second LSB in tags array indicates visited flag of a vertex
		if((tags[u] & 2) == 2 && (transTags[u] & 2) == 2) /* u is in both fwd and bwd reachability closure of one pivot => It is part of the SCC containing its pivot */
		{
			tags[u] = tags[u] | 1; // Setting eliminated bit of u to 1
			transTags[u] = transTags[u] | 1;
			color[u] = -1;
		}
		else
		{
			terminate = false; // a new subgraph is formed. Needs further processing 
			if((tags[u] & 2) == 0 && (transTags[u] & 2) == 0) // u is unreachable from its pivot
				color[u] = 3 * i;
			else if((tags[u] & 2) == 2 && (transTags[u] & 2) == 0) // u is only forward reachable from its pivot
				color[u] = 3 * i + 1;
			else if((tags[u] & 2) == 0 && (transTags[u] & 2) == 2)// u is only backward reachable from its pivot
				color[u] = 3 * i + 2;
					
			atomicMax((int32_t*)&maxColorVal, color[u]);
		}
	}
}

__global__ void colorWCCs_K2(int8_t *tags, int32_t *color, int32_t *wcc, int32_t N)
{
	int k = blockIdx.x * blockDim.x + threadIdx.x;
	if(k < N && (tags[k] & 1) == 0)
		color[k] = color[(wcc[k])];
} 


__global__ void colorWCCs_K1(int8_t *tags, int32_t *color, int32_t *wcc, int32_t V)
{
	int n = blockIdx.x * blockDim.x + threadIdx.x;
	if(n < V && (tags[n] & 1) == 0 && wcc[n] == n)
	{
		int old = atomicAdd((int32_t*)&maxColorVal, 1);
		color[n] = old + 1;
	}
}

__global__ void WCCinit(int32_t *wcc, int8_t* tags, int32_t N)
{
	int32_t i = blockIdx.x * blockDim.x + threadIdx.x;
	if(i < N && (tags[i] & 1) == 0)
		wcc[i] = i;
}


__global__ void kernel_print()
{
	printf("scc count=%d\nmaxColorVal%d\n oneSCCcount=%d\nmaxSCCSize%d\n",sccCount,maxColorVal,oneSCCcount,maxSCCSize);
}




int main()
{
	int32_t N,E;
	std::cin>>N;
	std::cin>>E;
	int32_t *nodes=(int32_t *)malloc(sizeof(int32_t)*(N+1));
	int32_t *edges=(int32_t *)malloc(sizeof(int32_t)*E);
	int32_t *transNodes=(int32_t *)malloc(sizeof(int32_t)*(N+1));
	int32_t *transEdges=(int32_t *)malloc(sizeof(int32_t)*E);
	int32_t *colors=(int32_t *)malloc(sizeof(int32_t)*N);
	int32_t *pivots=(int32_t *)malloc(sizeof(int32_t)*N);
	int8_t *tags=(int8_t *)malloc(sizeof(int8_t)*N);
	int8_t *transTags=(int8_t *)malloc(sizeof(int8_t)*N);
	int32_t *wcc = (int32_t*)malloc(sizeof(int32_t) * N);
	for(int32_t i=0;i<N;i++)
	{	pivots[i]=-1;
		nodes[i]=-1;
		transNodes[i]=-1;
		colors[i]=0;
		tags[i]=0;
		transTags[i]=0;
		wcc[i] = -1;
	}
	int32_t e=0,n1,n2;
	for(int32_t i=0;i<E;i++)
	{
		std::cin>>n1;
		std::cin>>n2;
		n1--;
		n2--;
		if(nodes[n1]==-1)
			nodes[n1]=e;

		edges[e]=n2;
		e++;
	}
	for(int32_t i=0;i<N;i++)
	{
		if(nodes[i]==-1)
		{
			int32_t j=i+1;
			while(j<N&&(nodes[j]==-1))
				j++;
			if(j<N)
				nodes[i]=nodes[j];
			else
				nodes[i]=E;
		}
	}
	e=0;
	for(int32_t i=0;i<E;i++)
	{
		std::cin>>n1;
		std::cin>>n2;
		n1--;
		n2--;
		if(transNodes[n2]==-1)
			transNodes[n2]=e;
		transEdges[e]=n1;
		e++;
	}
	for(int32_t i=0;i<N;i++)
	{
		if(transNodes[i]==-1)
		{
			int32_t j=i+1;
			while(j<N&&(transNodes[j]==-1))
				j++;
			if(j<N)
				transNodes[i]=transNodes[j];
			else
				transNodes[i]=E;
		}
	}
transNodes[N]=E;
nodes[N]=E;

//PRINTING
	/*
	for(int32_t i=0;i<N-1;i++)
	  {
	  int32_t offset=nodes[i+1]-nodes[i];
	  for(int32_t j=0;j<offset;j++)
	  {std::cout<<i<<"\t"<<edges[nodes[i]+j]<<"\n";
	  }
	  }
	  if(nodes[N-1]<E)
	  {
	  int i=nodes[N-1];
	//	std::cout<<i<<"\t"<<edges[i]<<"\n";
	while(i<E)
	{
	std::cout<<(N-1)<<"\t"<<edges[i]<<"\n";
	i++;
	}
	}
	for(int32_t i=0;i<N-1;i++)
	  {
	  int32_t offset=transNodes[i+1]-transNodes[i];
	  for(int32_t j=0;j<offset;j++)
	  {std::cout<<i<<"\t"<<transEdges[transNodes[i]+j]<<"\n";
	  }
	  }
	  if(transNodes[N-1]<E)
	  {
	  int i=transNodes[N-1];
	//	std::cout<<i<<"\t"<<edges[i]<<"\n";
	while(i<E)
	{
	std::cout<<(N-1)<<"\t"<<transEdges[i]<<"\n";
	i++;
	}
	}

*/
	//Kernel call
	int32_t *d_propagateForward,*d_terminate,*d_propagateBackward, *d_continueWcc, *d_pivots,*d_nodes,*d_edges,*d_transNodes,*d_transEdges,*d_colors, *d_wcc;
	int32_t *propagateForward=(int32_t *)malloc(sizeof(int32_t));
	int32_t *propagateBackward=(int32_t *)malloc(sizeof(int32_t));
	int32_t *terminate=(int32_t *)malloc(sizeof(int32_t));
	int32_t *continueWcc = (int32_t*)malloc(sizeof(int32_t));
	int8_t *d_tags,*d_transTags;
	cudaMalloc(&d_terminate, sizeof(int32_t));
	cudaMalloc(&d_propagateForward, sizeof(int32_t));
	cudaMalloc(&d_propagateBackward, sizeof(int32_t));
	cudaMalloc(&d_continueWcc, sizeof(int32_t));
	cudaMalloc(&d_propagateForward, sizeof(int32_t));
	cudaMalloc(&d_nodes, (N+1)*sizeof(int32_t));
	cudaMalloc(&d_transNodes, (N+1)*sizeof(int32_t));
	cudaMalloc(&d_transEdges, E*sizeof(int32_t));
	cudaMalloc(&d_edges, E*sizeof(int32_t));
	cudaMalloc(&d_colors, N*sizeof(int32_t));
	cudaMalloc(&d_pivots, N*sizeof(int32_t));
	cudaMalloc(&d_tags, N*sizeof(int8_t));
	cudaMalloc(&d_transTags, N*sizeof(int8_t));
	cudaMalloc(&d_wcc, N*sizeof(int32_t));

	cudaMemcpy(d_nodes, nodes,(N+1)*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_colors,colors,N*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_pivots,pivots,N*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_tags,tags,N*sizeof(int8_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_transTags,transTags,N*sizeof(int8_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_edges,edges,E*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_transNodes, transNodes,(N+1)*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_transEdges, transEdges,E*sizeof(int32_t), cudaMemcpyHostToDevice);
	cudaMemcpy(d_wcc, wcc, N*sizeof(int32_t), cudaMemcpyHostToDevice);
	
	*propagateForward=1;
	*propagateBackward=1;
	*terminate=0;
	*continueWcc = 1;
	Trim1<<<(N/1024)+1,1024>>>(d_colors, d_tags, d_transTags, d_nodes, d_transNodes, d_edges, d_transEdges, N);
	cudaDeviceSynchronize();
	PivotSelection<<<(N/1024)+1,1024>>>(d_colors , d_tags ,d_transTags, d_pivots, d_nodes , d_transNodes, d_edges, d_transEdges , N);
	cudaDeviceSynchronize();
	cudaMemcpy(pivots,d_pivots,sizeof(int32_t)*N, cudaMemcpyDeviceToHost);
	
	
/*	for(int i=0;i<N;i++)
	{
		printf("i %d\tpivot %d\n",i,pivots[i]);
	}*/
	setVisited<<<(N/1024)+1,1024>>>(d_pivots,d_tags,d_transTags,N);
	cudaDeviceSynchronize();
//creating stream
	cudaStream_t S1,S2;
	cudaStreamCreate(&S1);
	cudaStreamCreate(&S2);



	while( *propagateForward || *propagateBackward)	
	{
		if(*propagateForward)
		{
			//printf("********************Forwardbegin*****************\n");	
			cudaMemset(d_propagateForward, 0,sizeof(int32_t));
//			FWD_kernel<<<(N/1024)+1,1024,0,S1>>>(d_nodes,d_edges,d_colors,d_tags,d_propagateForward,N);
			warpCentricFWD<<<((N*WARPSIZE)/1024)+1,1024,0,S1>>>(d_nodes,d_edges,d_colors,d_tags,d_propagateForward,N);
		//	printf("********************ForwardEnd*****************\n");	
		}
		if(*propagateBackward)
		{

		//	printf("********************BackwardBegin*****************\n");	
	
			cudaMemset(d_propagateBackward, 0,sizeof(int32_t));
//			FWD_kernel<<<(N/1024)+1,1024,0,S2>>>(d_transNodes,d_transEdges,d_colors,d_transTags,d_propagateBackward,N);
			warpCentricFWD<<<((N*WARPSIZE)/1024)+1,1024,0,S2>>>(d_transNodes,d_transEdges,d_colors,d_transTags,d_propagateBackward,N);
		//	printf("********************BackwardEnd*****************\n");	
		}

		cudaDeviceSynchronize();
		cudaMemcpy(propagateForward , d_propagateForward , sizeof(int32_t), cudaMemcpyDeviceToHost);
		cudaMemcpy(propagateBackward , d_propagateBackward , sizeof(int32_t), cudaMemcpyDeviceToHost);
	}
		Update<<<(N/1024)+1,1024>>>(d_colors,d_tags,d_transTags, d_terminate, d_pivots, N);
	cudaDeviceSynchronize();

	//kernel_print<<<1,1>>>();

//printf("*******************************************\n");
	TrimTwo<<<((N*WARPSIZE)/1024)+1,1024>>>(d_colors, d_tags,d_transTags, d_nodes, d_edges, d_transNodes, d_transEdges, N);
	cudaDeviceSynchronize();

	//kernel_print<<<1,1>>>();
/*cudaMemcpy(tags,d_tags,sizeof(int8_t)*N, cudaMemcpyDeviceToHost);
	cudaMemcpy(transTags,d_transTags,sizeof(int8_t)*N, cudaMemcpyDeviceToHost);
	cudaMemcpy(colors,d_colors,sizeof(int32_t)*N, cudaMemcpyDeviceToHost);
		for(int i=0;i<N;i++)
	{
		std::cout<<"node "<<i<<"\teli  "<<(tags[i]&1)<<"\tcolor "<<colors[i]<<"\n";

	}
std::cout<<"****************************************************\n";
*/
	/*	for(int i=0;i<N;i++)
	{
	
		std::cout<<"node "<<i<<"\tvisited trans"<<(transTags[i]&2)<<"\tcolor "<<colors[i]<<"\n";

	}
*/

	Trim1<<<(N/1024)+1,1024>>>(d_colors,d_tags,d_transTags,d_nodes,d_transNodes, d_edges, d_transEdges, N);
	cudaDeviceSynchronize();
/*	
	while(*continueWcc)	
	{
		cudaMemset(d_continueWcc, 0, sizeof(int32_t));
		WCC_K1<<<(N/1024)+1,1024>>>(d_colors, d_tags, d_transTags,  d_nodes, d_edges, d_transNodes, d_transEdges, d_wcc, d_continueWcc, N);
		WCC_K2<<<(N/1024)+1,1024>>>(d_tags, d_wcc, d_continueWcc, N);
		cudaDeviceSynchronize();
		cudaMemcpy(continueWcc, d_continueWcc, sizeof(int32_t), cudaMemcpyDeviceToHost);
	}

	colorWCCs_K1<<<(N/1024)+1,1024>>>(d_tags, d_colors, d_wcc, N);
	colorWCCs_K2<<<(N/1024)+1,1024>>>(d_tags, d_colors, d_wcc, N);

*/

	cudaMemcpy(tags,d_tags,sizeof(int8_t)*N, cudaMemcpyDeviceToHost);
	cudaMemcpy(transTags,d_transTags,sizeof(int8_t)*N, cudaMemcpyDeviceToHost);
	cudaMemcpy(colors,d_colors,sizeof(int32_t)*N, cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();
/*	for(int i=0;i<N;i++)
	{
		std::cout<<"node "<<i<<"\tvisited  "<<(tags[i]&2)<<"\tcolor "<<colors[i]<<"\n";

	}
	std::cout<<"****************************************************\n";
	for(int i=0;i<N;i++)
	{
	
		std::cout<<"node "<<i<<"\tvisited trans"<<(transTags[i]&2)<<"\tcolor "<<colors[i]<<"\n";

	}*/

	//	for(int32_t i=0;i<E;i++)
	//		edges[i]=i;
	std::cout<<"Number of nodes:"<<N<<"\nNumber of edges:"<<E<<"\n";
}
