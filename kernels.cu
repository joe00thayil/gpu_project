#include <cuda.h>
#include <cuda_runtime.h>

#define WARPSIZE 32

volatile int maxColorVal(0); // Stores the current maximum value of the colors used so far
volatile int oneSCCcount(0);
volatile int sccCount(0); // total count of SCCs in G
volatile int maxSCCSize(1); // Size of the maximum sized SCC, Initialized to 1


__global__ void Trim1(int32_t *color, int8_t *tags, int* nodes, int* transNodes, int N)
{
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < N)
	{
	int a = nodes[id + 1] - nodes[id]; // Outdegree of vertex id in G
	int b =  transNodes[id + 1] - transNodes[id]; // Outdegree of vertex id in G transpose == Indegree in G
	if(a == 0 || b == 0)
	{
		// 1-SCC identified
//		color[id] = atomicAdd(&maxColorVal, 1); // No need to color an SCC
		tags[id] = tags[id] | 1; // LSB of tag represents eliminated bit. Setting it to 1
		atomicAdd(&oneSCCcount, 1);
		atomicAdd(&sccCount, 1);
	}
	}
}


__global__ void PivotSelection(int32_t *color, int8_t *tags, int *pivots, int *nodes, int* transNodes, int N)
{
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id < N)
	{
	if((tags[id] & 1) == 0) // The vertex is not yet eliminated
	{
		int subGraphId = color[id];
		int currentPivot, old, curProd;
		int newProd = (nodes[id + 1] - nodes[id]) * (transNodes[id + 1] - transNodes[id]); // product of in and out degrees of the new candidate for pivot
		do{
			currentPivot = pivots[subGraphId % N];
			curProd = (nodes[currentPivot + 1] - nodes[currentPivot]) *  (transNodes[currentPivot + 1] - transNodes[currentPivot]); // product of in and out degrees for currentPivot
			if(currentPivot == -1 || newProd > curProd)
			{
				old = atomicCAS(&(pivots[subGraphId % N]), currentPivot, id); 
			}
			else
			{
				old = currentPivot;
		
			}
		}while(old != currentPivot);

	}
	}

}

// NOTE : Before calling FWD and BWD kernels, clear visited bit field in tags array. Also, before calling pivot selection, reset pivots array to -1 always
__global__ void Update(int* color, int8_t* tags, int8_t* transTags, int* terminate, int N)
{
	unsigned int u = blockIdx.x * blockDim.x + threadIdx.x;
	if(u < N && (tags[u] & 1) == 0) // Proceed only if the vertex u is not yet eliminated
	{
		int i = color[u];
		if(u == i) // u is a pivot
		{
			atomicAdd(&sccCount, 1); /*TO-DO: Check how to track max sized SCC's size. This may be a 1-SCC too. Need to add to oneSCCcount then*/
		}
		// Second LSB in tags array indicates visited flag of a vertex
		if((tags[u] & 2) == 2 && (transTags[u] & 2) == 2) /* u is in both fwd and bwd reachability closure of one pivot => It is part of the SCC containing its pivot */
		{
			tags[u] = tags[u] | 1; // Setting eliminated bit of u to 1
		}
		else
		{
			terminate = false; // a new subgraph is formed. Needs further processing 
			if((tags[u] & 2) == 0 && (transTags[u] & 2) == 0) // u is unreachable from its pivot
				color[u] = 3 * i;
			else if((tags[u] & 2) == 2 && (transTags[u] & 2) == 0) // u is only forward reachable from its pivot
				color[u] = 3 * i + 1;
			else if((tags[u] & 2) == 0 && (transTags[u] & 2) == 2)// u is only backward reachable from its pivot
				color[u] = 3 * i + 2;
		}
	}
}


__global__ void TrimTwo(int32_t *color, int8_t *tags, int32_t* nodes, int32_t* edges, int32_t* transNodes, int32_t* transEdges, int32_t V)
{
	unsigned int n = blockIdx.x * blockDim.x + threadIdx.x;
	if(n < V)
	{
		int inDegree_n = transNodes[n+1] - transNodes[n];
		int outDegree_n = nodes[n+1] - nodes[n];
		// Identifying if n is part of a 2-SCC
		if(inDegree_n == 1)
		{
			int k = transEdges[(transNodes[n])]; // k is the only in-neighbour of n
			if(color[n] == color[k]) // n and k are in the same subgraph
			{
				int inDegree_k = transNodes[k+1] - transNodes[k];
				if(inDegree_k == 1)
				{
					int inNbrOf_k = transEdges[(transNodes[k])];
					if(inNbrOf_k == n) // n is the only in-neighbour of k
					{
						// k and n form a 2-SCC
						tags[n] = tags[n] | 1; // Eliminated bit to 1
						tags[k] = tags[k] | 1;
						atomicAdd(&sccCount, 1);
						atomicMax(&maxSCCSize, 2);	
					}
					
				}
				
			}
		}
		else if(outDegree_n == 1)
		{
			int k = edges[(nodes[n])]; // k is the only out-neighbour of n
			if(color[n] == color[k]) // n and k are in the same subgraph
			{
				int outDegree_k = nodes[k+1] - nodes[k];
				if(outDegree_k == 1)
				{
					int outNbrOf_k = edges[(nodes[k])];
					if(outNbrOf_k == n) // n is the only out-neighbour of k
					{
						// k and n form a 2-SCC
						tags[n] = tags[n] | 1; // Eliminated bit to 1
                                                tags[k] = tags[k] | 1;
                                                atomicAdd(&sccCount, 1);
                                                atomicMax(&maxSCCSize, 2);
					}
				}
			}
		}
	}
}


__global__ void warpCentricFWD(int32_t* color, int8_t* tags, int32_t* nodes, int32_t* edges, int* propagate, int N)
{
	unsigned int v = blockIdx.x * blockDim.x + threadIdx.x;
	if(v < N)
	{
		int w = v / WARPSIZE;
		int eliminated_w = tags[w] & 1;
		int visited_w =  tags[w] & 2;
		if(eliminated_w == 0 && visited_w == 2)
		{
			int nbrCount = nodes[w+1] - nodes[w];
			int nbrBegin = nodes[w];
			for(int i = (v % WARPSIZE); i < nbrCount; i += WARPSIZE)
			{
				int u = edges[nbrBegin + i];
				int eliminated_u = tags[u] & 1;
				int visited_u = tags[u] & 2;
				if(eliminated_u == 0 && visited_u == 0 && color[u] == color[w])
				{
					tags[u] = tags[u] | 2; // Set visited to true
					*propagate = 1;
				}
			}
		}
	}
}
